import React from 'react';

class Home extends React.Component {
    render() {
        return(
            <div className="home">
                <h1>Welcome!</h1>
                <p>
                    This application is designed to allow the user to query Finnhub's stock data API and return information about the company that matches that stock ticker.
                </p>
                <p>
                    To begin, click login in the navbar above!
                </p>
                <p>
                    Desired improvements and enhancements (TODO):
                </p>
                <ul>
                    <li>
                        Create history of stock ticker queries and allow the user to easily access
                        that query again in the future with a click of a button.
                    </li>
                    <li>
                        UI/UX improvements by leveraging more Bootstrap 4 components and CSS.
                    </li>
                    <li>
                        Implement a CSS preprocessor for more organized CSS, cleaner code.
                    </li>
                    <li>
                        Allow different types of searches. For example, search for a particular industry and 
                        return the top 5-10 companies in that industry. Allow for searches on combined filters
                        such as "Find top 10 biomedical firms in Japan".
                    </li>
                    <li>
                        Export results to file (csv or text).
                    </li>
                </ul>
            </div>
        )
    }
}

export default Home;