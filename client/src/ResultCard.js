import React from 'react';

class ResultCard extends React.Component {
  render() {
    return(
      <div className="card">
        <img src={this.props.data.logo} className="card-img-top" alt="Company Logo" />
        <div className="card-body">
          <h5 className="card-title">{this.props.data.name}</h5>
          <p className="card-text"><b>Industry:</b><br /> {this.props.data.finnhubIndustry}</p>
          <p className="card-text"><b>Market Cap:</b><br /> {this.props.data.marketCapitalization}</p>
          <p className="card-text"><b>Outstanding Shares:</b><br /> {this.props.data.shareOutstanding}</p>
          <p className="card-text"><a target="_blank" href={this.props.data.weburl}>Website</a></p>
        </div>
      </div>
    )
  }
}

export default ResultCard;