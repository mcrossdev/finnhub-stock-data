import React from 'react';
import ResultCard from './ResultCard';

class StockQuery extends React.Component {

  constructor(props) {
    super(props);
    this.state = {tickerQuery: '', doesResultExist: false, tickerResult: ''};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({tickerQuery: event.target.value});
  }

  handleSubmit (event) {
    this.callAPI(this.state.tickerQuery);
    event.preventDefault();
  }

  callAPI(tickerQuery) {
    fetch("https://finnhub.io/api/v1/stock/profile2?token=bs613tfrh5ren4qkr59g&symbol=" + tickerQuery)
      .then(res => res.json())
      .then(res => this.setState({ tickerResult: res, doesResultExist: true }));
  }

  render() {
    const doesResultExist = this.state.doesResultExist;
    let card;
    if (doesResultExist) {
      card = <ResultCard data={this.state.tickerResult}></ResultCard>;
    }

    return(
      <div className="stockQuery">
        <form onSubmit={this.handleSubmit}>
          <label>
            Ticker:
          </label>
          <input type="text" placeholder="Enter Ticker Query Here..." value={this.state.tickerQuery} onChange={this.handleChange}>

          </input>
          <input type="submit" value="Submit"></input>
        </form> 
        {card}        
      </div>
    )
  }
}

export default StockQuery;