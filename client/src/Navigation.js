import React from 'react';
import { Route, Link, Switch } from 'react-router-dom';
import Home from './Home';
import Login from './Login';
import Register from './Register';

class Navigation extends React.Component {
    render() {
        return(
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <Link to="/">Home</Link>
                    </li>
                    <li class="nav-item">
                        <Link to="/login">Login</Link>
                    </li>
                    <li class="nav-item">
                        <Link to="/register">Register</Link>
                    </li>
                </ul>
                </div>
            </nav>
            <Switch>
                <Route exact path="/" component={Home}>Home</Route>
                <Route to="/login" component={Login}>Login</Route>
                <Route path="/register" component={Register}>Register</Route>
            </Switch>
        )
    }
}

export default Navigation;