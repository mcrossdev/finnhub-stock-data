import React from 'react';
import './App.css';
import Home from './Home';
import Navigation from './Navigation';

class App extends React.Component {

  render() {
    return (
      <div className="app">
        <Navigation></Navigation>
        <div className="main">
          <Home></Home>
        </div>
      </div>
    )
  }
}

export default App;